#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys

# Constantes. Puerto.



class SIPRequest():

    def __init__(self, data):
        self.data = data

    def parse(self):
        self._parse_command(self.data.decode('utf-8'))
        cabecera = self.data.splitlines()[1:]
        self._parse_headers(cabecera)

    def _get_address(self, uri):
        self.uri=uri
        schema = uri.split(':')[0]
        address = uri.split(':')[1]

        return address, schema

    def _parse_command(self, line):

        partes = line.split()
        self.command =partes[0]
        self.uri =partes[1]
        partesuri = self._get_address(self.uri)
        self.adress = partesuri[0]
        schema = partesuri[1]

        if self.command == "REGISTER" and schema == "sip":
            self.result = "200 OK"
        elif self.command != "REGISTER":
            self.result = "405 Method Not Allowed"
        elif schema != "sip:":
            self.result = "416 Unsupported URI Scheme"

    def _parse_headers(self, first_nl):
        self.headers = {}
        for headerline in first_nl:
            header= headerline.split(':')
            head = header[0]
            value = header[1]
            self.headers[head] = value


class SIPRegisterHandler(socketserver.BaseRequestHandler):
    DiccIP = {}

    def process_register(self, sip_request):
        if sip_request.headers["Expires"] == "0" and self.DiccIP != {}:
            del self.DiccIP[sip_request.address]
        elif sip_request.headers["Expires"] == "0":
            self.DiccIP = self.DiccIP
        else:
            self.DiccIP[sip_request.address] = f"{self.client_address[0]}"

        print(f"usuarios registrados: {self.DiccIP}")

    def handle(self):
        data = self.request[0]
        sock = self.request[1]
        sip_request = SIPRequest(data)
        sip_request.parse()
        if (sip_request.command == "REGISTER") and (sip_request.result == "200 OK"):
            self.process_register(sip_request)
        sock.sendto(f"SIP/2.0 {sip_request.result}\r\n\r\n".encode(), self.client_address)


def main():
    PORT = int(sys.argv[1])
    # Listens at port PORT (my address)
    # and calls the EchoHandler class to manage the request
    try:
        serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler)
        print(f"Server listening in port: {PORT}...")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()

